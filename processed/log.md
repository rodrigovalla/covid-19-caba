# Errors during data processing

## Cases dataset:

### 2021-04-10:
Errors related to COVID-19 cases dataset:  
- !!! Errors while processing by age: 8799 (0.43%)
- !!! Errors while processing by zone: 46619 (2.27%)
- !!! Errors while processing delays: 499 (0.02%)

### 2021-04-14:
Errors related to COVID-19 cases dataset:  
- !!! Errors while processing by age: 5396 (0.26%)
- !!! Errors while processing by zone: 44142 (2.11%)
- !!! Errors while processing delays: 537 (0.03%)

### 2021-04-17:
-- !!! Errors while processing by age: 9632 (0.45%)
-- !!! Errors while processing by zone: 49257 (2.32%)
-- !!! Errors while processing delays: 698 (0.03%)

### 2021-04-19:
-- !!! Errors while processing by age: 5619 (0.26%)
-- !!! Errors while processing by zone: 46493 (2.15%)
-- !!! Errors while processing delays: 1007 (0.05%)

### 2021-04-24:
-- !!! Errors while processing by age: 4808 (0.22%)
-- !!! Errors while processing by zone: 46503 (2.1%)
-- !!! Errors while processing delays: 1100 (0.05%)

### 2021-05-01:
-- !!! Errors while processing by age: 5429 (0.24%)
-- !!! Errors while processing by zone: 48453 (2.12%)
-- !!! Errors while processing by origin: 2449 (0.11%)
-- !!! Errors while processing delays: 1238 (0.05%)

### 2021-05-15:
-- !!! Errors while processing by age: 3817 (0.16%)
-- !!! Errors while processing by zone: 49368 (2.05%)
-- !!! Errors while processing by origin: 2432 (0.1%)
-- !!! Errors while processing delays: 1398 (0.06%)

### 2021-05-20:
-- !!! Errors while processing by age: 5053 (0.21%)
-- !!! Errors while processing by zone: 52188 (2.15%)
-- !!! Errors while processing by origin: 2431 (0.1%)
-- !!! Errors while processing delays: 1430 (0.06%)

### 2021-06-03:
-- !!! Errors while processing by age: 4715 (0.18%)
-- !!! Errors while processing by zone: 55142 (2.14%)
-- !!! Errors while processing by origin: 2222 (0.09%)
-- !!! Errors while processing delays: 1652 (0.06%)

### 2021-06-12:
-- !!! Errors while processing by age: 4925 (0.19%)
-- !!! Errors while processing by zone: 56721 (2.15%)
-- !!! Errors while processing by origin: 2170 (0.08%)
-- !!! Errors while processing delays: 1828 (0.07%)

### 2021-06-19:
-- !!! Errors while processing by age: 6032 (0.23%)
-- !!! Errors while processing by zone: 58475 (2.19%)
-- !!! Errors while processing by origin: 2024 (0.08%)
-- !!! Errors while processing delays: 1945 (0.07%)

### 2021-06-25:
-- !!! Errors while processing by age: 5224 (0.19%)
-- !!! Errors while processing by zone: 57734 (2.12%)
-- !!! Errors while processing by origin: 1936 (0.07%)
-- !!! Errors while processing delays: 2088 (0.08%)

### 2021-07-04:
-- !!! Errors while processing by age: 4873 (0.18%)
-- !!! Errors while processing by zone: 59544 (2.14%)
-- !!! Errors while processing by origin: 1695 (0.06%)
-- !!! Errors while processing delays: 2214 (0.08%)

### 2021-07-14:
-- !!! Errors while processing by age: 5180 (0.18%)
-- !!! Errors while processing by zone: 60975 (2.14%)
-- !!! Errors while processing by origin: 1820 (0.06%)
-- !!! Errors while processing delays: 2285 (0.08%)

### 2021-07-20:
-- !!! Errors while processing by age: 4007 (0.14%)
-- !!! Errors while processing by zone: 61563 (2.13%)
-- !!! Errors while processing by origin: 1681 (0.06%)
-- !!! Errors while processing delays: 2304 (0.08%)

### 2021-08-09:
-- !!! Errors while processing by age: 4483 (0.15%)
-- !!! Errors while processing by zone: 64678 (2.15%)
-- !!! Errors while processing by origin: 1631 (0.05%)
-- !!! Errors while processing delays: 2366 (0.08%)

### 2021-08-16:
-- !!! Errors while processing by age: 3885 (0.13%)
-- !!! Errors while processing by zone: 64364 (2.11%)
-- !!! Errors while processing by origin: 1550 (0.05%)
-- !!! Errors while processing delays: 683 (0.02%)

### 2021-09-03:
-- !!! Errors while processing by age: 5881 (0.19%)
-- !!! Errors while processing by zone: 69712 (2.19%)
-- !!! Errors while processing by origin: 1591 (0.05%)
-- !!! Errors while processing delays: 698 (0.02%)

### 2021-09-18:
-- !!! Errors while processing by age: 2599 (0.08%)
-- !!! Errors while processing by zone: 76462 (2.34%)
-- !!! Errors while processing by origin: 1519 (0.05%)
-- !!! Errors while processing delays: 727 (0.02%)

### 2021-09-28:
-- !!! Errors while processing by age: 3544 (0.11%)
-- !!! Errors while processing by zone: 71999 (2.18%)
-- !!! Errors while processing by origin: 1512 (0.05%)
-- !!! Errors while processing delays: 737 (0.02%)

### 2021-10-06:
-- !!! Errors while processing by age: 3349 (0.1%)
-- !!! Errors while processing by zone: 73940 (2.21%)
-- !!! Errors while processing by origin: 1506 (0.05%)
-- !!! Errors while processing delays: 749 (0.02%)

### 2021-10-13:
-- !!! Errors while processing by age: 3907 (0.12%)
-- !!! Errors while processing by zone: 75728 (2.24%)
-- !!! Errors while processing by origin: 1542 (0.05%)
-- !!! Errors while processing delays: 746 (0.02%)

### 2021-10-26:
-- !!! Errors while processing by age: 4074 (0.12%)
-- !!! Errors while processing by zone: 79181 (2.31%)
-- !!! Errors while processing by origin: 1554 (0.05%)
-- !!! Errors while processing delays: 769 (0.02%)

### 2021-11-25:
-- !!! Errors while processing by age: 3869 (0.11%)
-- !!! Errors while processing by zone: 84001 (2.37%)
-- !!! Errors while processing by origin: 1580 (0.04%)
-- !!! Errors while processing delays: 795 (0.02%)

### 2021-12-03:
-- !!! Errors while processing by age: 4705 (0.13%)
-- !!! Errors while processing by zone: 86667 (2.43%)
-- !!! Errors while processing by origin: 1608 (0.05%)
-- !!! Errors while processing delays: 807 (0.02%)

### 2021-12-11:
-- !!! Errors while processing by age: 4914 (0.14%)
-- !!! Errors while processing by zone: 88039 (2.45%)
-- !!! Errors while processing by origin: 1645 (0.05%)
-- !!! Errors while processing delays: 830 (0.02%)

### 2021-12-23:
-- !!! Errors while processing by age: 5741 (0.16%)
-- !!! Errors while processing by zone: 90865 (2.49%)
-- !!! Errors while processing by origin: 1796 (0.05%)
-- !!! Errors while processing delays: 880 (0.02%)

### 2022-01-06:
-- !!! Errors while processing by age: 1753940 (46.72%)
-- !!! Errors while processing by zone: 1765716 (47.03%)
-- !!! Errors while processing by origin: 399225 (10.63%)
-- !!! Errors while processing delays: 200 (0.01%)

### 2022-01-09:
-- !!! Errors while processing by age: 8900 (0.24%)
-- !!! Errors while processing by zone: 75073 (1.99%)
-- !!! Errors while processing by origin: 5003 (0.13%)
-- !!! Errors while processing delays: 1588 (0.04%)

### 2022-01-18:
-- !!! Errors while processing by age: 8182 (0.21%)
-- !!! Errors while processing by zone: 65717 (1.7%)
-- !!! Errors while processing by origin: 5319 (0.14%)
-- !!! Errors while processing delays: 1975 (0.05%)

### 2022-01-24:
-- !!! Errors while processing by age: 8779 (0.22%)
-- !!! Errors while processing by zone: 67316 (1.72%)
-- !!! Errors while processing by origin: 6361 (0.16%)
-- !!! Errors while processing delays: 2239 (0.06%)

### 2022-02-05:
-- !!! Errors while processing by age: 5923 (0.15%)
-- !!! Errors while processing by zone: 68481 (1.7%)
-- !!! Errors while processing by origin: 3155 (0.08%)
-- !!! Errors while processing delays: 3066 (0.08%)

### 2022-02-14:
-- !!! Errors while processing by age: 3914 (0.1%)
-- !!! Errors while processing by zone: 70769 (1.73%)
-- !!! Errors while processing by origin: 2095 (0.05%)
-- !!! Errors while processing delays: 3207 (0.08%)

### 2022-02-21:
-- !!! Errors while processing by age: 6366 (0.15%)
-- !!! Errors while processing by zone: 73653 (1.78%)
-- !!! Errors while processing by origin: 2390 (0.06%)
-- !!! Errors while processing delays: 3217 (0.08%)

### 2022-03-26:
-- !!! Errors while processing by age: 5211 (0.12%)
-- !!! Errors while processing by zone: 87692 (2.04%)
-- !!! Errors while processing by origin: 2200 (0.05%)
-- !!! Errors while processing delays: 3210 (0.07%)

### 2022-05-24:
-- !!! Errors while processing by age: 5072 (0.11%)
-- !!! Errors while processing by zone: 97873 (2.21%)
-- !!! Errors while processing by origin: 2722 (0.06%)
-- !!! Errors while processing delays: 3213 (0.07%)
